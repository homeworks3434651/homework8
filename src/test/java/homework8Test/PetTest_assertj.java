package homework8Test;

import org.homework8.*;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

public class PetTest_assertj {

    @Test
    public void testEat() {
        // Create a concrete subclass of Pet (e.g., Dog)
        Dog pet = new Dog("Buddy", 4);

        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the eat() method
        pet.eat();

        // Reset the standard output stream
        System.setOut(new PrintStream(Objects.requireNonNullElseGet(System.out, () -> new PrintStream(System.out))));

        // Check only the output related to the eat() method
        String expectedOutput = "I eat meat.";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput).isEqualTo(expectedOutput);
    }

    @Test
    public void testRespond() {
        // Create a concrete subclass of Pet (e.g., Dog)
        Dog pet = new Dog("Buddy", 4);
        pet.setNickname("TestPet");

        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the respond() method
        pet.respond();

        // Reset the standard output stream
        System.setOut(new PrintStream(Objects.requireNonNullElseGet(System.out, () -> new PrintStream(System.out))));

        // Check only the output related to the respond() method
        String expectedOutput = "Woof! I'm a dog.";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput).isEqualTo(expectedOutput);
    }

    @Test
    public void testFoul() {
        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Create a concrete subclass of Pet (e.g., Dog)
        Dog pet = new Dog("Buddy", 4);
        pet.foul();

        // Reset the standard output stream
        System.setOut(new PrintStream(Objects.requireNonNullElseGet(System.out, () -> new PrintStream(System.out))));

        // Check only the output related to the foul() method
        String expectedOutput = "I left a little surprise for you on the carpet.";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput).contains(expectedOutput);
    }

    @Test
    public void testEquals_and_hashCode() {
        Set<String> habits1 = new HashSet<>(Set.of("play", "sleep"));
        Set<String> habits2 = new HashSet<>(Set.of("play", "sleep"));
        Set<String> habits3 = new HashSet<>(Set.of("play", "rollover"));

        DomesticCat pet1 = new DomesticCat("Whiskers", 3);
        pet1.setTrickLevel(50);
        pet1.setHabits(habits1);
        DomesticCat pet2 = new DomesticCat("Whiskers", 3);
        pet2.setTrickLevel(50);
        pet2.setHabits(habits2);
        DomesticCat pet3 = new DomesticCat("Thomas", 2);
        pet3.setTrickLevel(60);
        pet3.setHabits(habits3);

        // Testing reflexivity
        assertThat(pet1).isEqualTo(pet1);

        // Testing consistency
        assertThat(pet1).isEqualTo(pet2);

        // Testing symmetry
        assertThat(pet2).isEqualTo(pet1);

        // Testing transitivity
        assertThat(pet1).isEqualTo(pet2).isEqualTo(pet1).isEqualTo(pet2);

        // Testing equality with null
        assertThat(pet1).isNotEqualTo(null);

        // Testing hash code consistency
        assertThat(pet1.hashCode()).isEqualTo(pet2.hashCode());

        // Testing hash code inequality with different objects
        assertThat(pet1.hashCode()).isNotEqualTo(pet3.hashCode());

        // Override hashCode for pet1 to introduce a discrepancy
        pet1 = new DomesticCat("Whiskers", 3) {
            @Override
            public int hashCode() {
                return Objects.hash(getAge());
            }
        };
        pet1.setTrickLevel(50);
        pet1.setHabits(habits1);
        assertThat(pet1).isEqualTo(pet2);
        assertThat(pet1.hashCode()).isNotEqualTo(pet2.hashCode());

        // Changing a field after object creation
        pet1.setAge(4);
        assertThat(pet1).isNotEqualTo(pet2);
    }

    @Test
    public void testEqualsWhenSpeciesIsUnknown() {
        Set<String> habits1 = new HashSet<>(Set.of("play", "sleep"));
        Set<String> habits2 = new HashSet<>(Set.of("play", "sleep"));

        // Create two Pet objects with UNKNOWN species
        Pet pet1 = new TestPet("Buddy", 3);
        pet1.setTrickLevel(50);
        pet1.setHabits(habits1);
        Pet pet2 = new TestPet("Buddy", 3);
        pet2.setTrickLevel(50);
        pet2.setHabits(habits2);

        // Ensure the equals method works as expected
        assertThat(pet1).isEqualTo(pet2);
        assertThat(pet2).isEqualTo(pet1);
    }

    @Test
    public void toStringShouldReturnFormattedString() {
        Dog dog = new Dog("Buddy", 4);

        String expected1 = String.format("Pet{%n" +
                        "    species=DOG%n" +
                        "    canFly=false%n" +
                        "    hasFur=true%n" +
                        "    numberOfLegs=4%n" +
                        "    nickname=Buddy%n" +
                        "    age=4%n" +
                        "    trickLevel=0%n" +
                        "    habits=%s%n" +
                        "}",
                "[]");

        String actual1 = dog.toString();

        assertThat(actual1).isEqualTo(expected1);

        Fish pet = new Fish("Polly", 3);

        String expected2 = String.format("Pet{%n" +
                        "    species=FISH%n" +
                        "    canFly=false%n" +
                        "    hasFur=false%n" +
                        "    numberOfLegs=0%n" +
                        "    nickname=Polly%n" +
                        "    age=3%n" +
                        "    trickLevel=0%n" +
                        "    habits=%s%n" +
                        "}",
                "[]");

        String actual2 = pet.toString();

        assertThat(actual2).isEqualTo(expected2);
    }

    @Test
    public void testToStringWhenSpeciesIsUnknown() {
        Set<String> habits = new LinkedHashSet<>(Arrays.asList("play", "sleep"));

        // Create a Pet object with UNKNOWN species
        Pet pet = new TestPet("Buddy", 3);
        pet.setTrickLevel(50);
        pet.setHabits(habits);

        // Define the expected output
        String expectedOutput = String.format("Pet{%n" +
                        "    species=%s%n" +
                        "    canFly=%s%n" +
                        "    hasFur=%s%n" +
                        "    numberOfLegs=%d%n" +
                        "    nickname=%s%n" +
                        "    age=%d%n" +
                        "    trickLevel=%d%n" +
                        "    habits=%s%n" +
                        "}",
                Species.UNKNOWN,
                Species.UNKNOWN.canFly(),
                Species.UNKNOWN.hasFur(),
                Species.UNKNOWN.getNumberOfLegs(),
                "Buddy",
                3,
                50,
                "[play, sleep]");

        // Ensure the toString method works as expected
        assertThat(pet.toString()).isEqualTo(expectedOutput);
    }
}
