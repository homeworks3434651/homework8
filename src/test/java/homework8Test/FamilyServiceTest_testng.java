package homework8Test;

import org.homework8.*;
import org.junit.jupiter.api.*;
import org.testng.Assert;

import java.io.*;
import java.util.*;

public class FamilyServiceTest_testng {

    private CollectionFamilyDao mockFamilyDao;
    private FamilyService familyService;
    private PrintStream originalOut;

    @BeforeEach
    void setUp() {
        // Save the original System.out before redirecting
        originalOut = System.out;

        // Create a familyService with a mocked FamilyDao
        mockFamilyDao = new CollectionFamilyDao();
        familyService = new FamilyService(mockFamilyDao);
    }

    @AfterEach
    void tearDown() {
        System.setOut(originalOut);
    }

    Family family1 = TestUtilities.setUpFamily1();
    Family family2 = TestUtilities.setUpFamily2();

    @Test
    void testGetAllFamilies() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call getAllFamilies() method
        List<Family> allFamilies = familyService.getAllFamilies();

        // Verify the result
        Assert.assertEquals(allFamilies.size(), 2);
        Assert.assertTrue(allFamilies.containsAll(Arrays.asList(family1, family2)));
    }

    @Test
    void testGetFamilyByIndex() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call getFamilyByIndex() method
        Family retrievedFamily = familyService.getFamilyByIndex(0);

        // Verify the result
        Assert.assertNotNull(retrievedFamily);
        Assert.assertEquals(retrievedFamily, family1);
    }

    @Test
    void testDeleteFamily() {
        mockFamilyDao.saveFamily(family1);

        // Call deleteFamily() method
        boolean deleted = familyService.deleteFamily(family1);

        // Verify the result
        Assert.assertTrue(deleted);  // This assertion is failing
        Assert.assertEquals(familyService.getAllFamilies().size(), 0);
    }

    @Test
    void testDeleteNonExistingFamily() {
        // Call deleteFamily() method
        boolean deleted = familyService.deleteFamily(family1);

        // Verify the result
        Assert.assertFalse(deleted);
        Assert.assertEquals(familyService.getAllFamilies().size(), 0);
    }

    @Test
    void testDeleteFamilyByIndex() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call deleteFamilyByIndex() method
        Assert.assertTrue(familyService.deleteFamily(0));

        // Verify the result
        Assert.assertEquals(familyService.getAllFamilies().size(), 1);
        Assert.assertFalse(familyService.getAllFamilies().contains(family1));
        Assert.assertTrue(familyService.getAllFamilies().contains(family2));
    }

    @Test
    void testDeleteNonExistingFamilyByIndex() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call deleteFamilyByIndex() with a non-existing index
        Assert.assertFalse(familyService.deleteFamily(2));

        // Verify the result (no change in families)
        Assert.assertEquals(familyService.getAllFamilies().size(), 2);
        Assert.assertTrue(familyService.getAllFamilies().containsAll(Arrays.asList(family1, family2)));
    }

    @Test
    void testDeleteFamilyByNegativeIndex() {
        mockFamilyDao.saveFamily(family1);

        // Call deleteFamilyByIndex() with a negative index
        Assert.assertFalse(familyService.deleteFamily(-1));

        // Verify the result (no change in families)
        Assert.assertEquals(familyService.getAllFamilies().size(), 1);
        Assert.assertTrue(familyService.getAllFamilies().contains(family1));
    }

    @Test
    void testSaveNewFamily() {
        familyService.saveFamily(family1);

        // Verify the result
        Assert.assertEquals(familyService.getAllFamilies().size(), 1);
        Assert.assertTrue(familyService.getAllFamilies().contains(family1));
    }

    @Test
    void testUpdateExistingFamily() {
        mockFamilyDao.saveFamily(family1);

        // Update the existing family with the same members
        family1.getFather().setYear(1980);
        family1.getMother().setYear(1985);

        // Call saveFamily() method with the updated family
        familyService.saveFamily(family1);

        // Verify the result (existing family should be updated, not added)
        Assert.assertEquals(familyService.getAllFamilies().size(), 1);
        Assert.assertTrue(familyService.getAllFamilies().contains(family1));
    }

    @Test
    void testSaveNullFamily() {
        // Call saveFamily() method with null family
        familyService.saveFamily(null);

        // Verify the result (no change in families)
        Assert.assertEquals(familyService.getAllFamilies().size(), 0);
    }

    @Test
    void testDisplayAllFamilies() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Verify the printed output
        String expectedOutput = String.format(
                "%nDisplaying all families:%n%s%n%s%n%n",
                family1, family2
        );

        // Test displayAllFamilies with families
        testDisplayAllFamiliesWithOutput(expectedOutput);
    }

    @Test
    public void testDisplayAllFamiliesEmptyList() {
        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call displayAllFamilies() method with an empty list
        familyService.displayAllFamilies();

        // Test displayAllFamilies with an empty list
        String expectedOutput = String.format("Displaying all families:%nThe list of families is empty.").trim();

        Assert.assertEquals(outContent.toString().trim(), expectedOutput);
    }

    private void testDisplayAllFamiliesWithOutput(String expectedOutput) {
        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call displayAllFamilies() method
        familyService.displayAllFamilies();

        // Verify the printed output
        Assert.assertEquals(expectedOutput, outContent.toString());

        // Reset System.out
        System.setOut(originalOut);
    }

    @Test
    void testGetFamiliesBiggerThan() {
        testFamilyFilteringMethod(3, true);
    }

    @Test
    void testGetFamiliesLessThan() {
        testFamilyFilteringMethod(4, false);
    }

    private void testFamilyFilteringMethod(int numberOfMembers, boolean isBiggerThan) {
        family1.addChild(new Human("Bob", "Doe", 1995));
        family1.addChild(new Human("Davida", "Doe", 2000, family1));
        mockFamilyDao.saveFamily(family1);

        Human child_family2 = new Human("Ryan", "Smith", 2004, family2);
        family2.addChild(child_family2);
        mockFamilyDao.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the appropriate family filtering method
        List<Family> resultFamilies = isBiggerThan ?
                familyService.getFamiliesBiggerThan(numberOfMembers) :
                familyService.getFamiliesLessThan(numberOfMembers);

        // Verify the result and printed output
        Assert.assertEquals(resultFamilies.size(), 1);
        Assert.assertTrue(resultFamilies.contains(isBiggerThan ? family1 : family2));

        String isBiggerThanString = """
                [Family{
                  Father: Man{name='John', surname='Doe', year=1970, iq=0, schedule=[]}
                  Mother: Woman{name='Jane', surname='Doe', year=1975, iq=0, schedule=[]}
                  Children: [Human{name='Bob', surname='Doe', year=1995, iq=0, schedule=[]}, Human{name='Davida', surname='Doe', year=2000, iq=0, schedule=[]}]
                  Pets: []
                  Total Persons in Family: 4
                }]""";

        String isLessThanString = """
                [Family{
                  Father: Man{name='Bob', surname='Smith', year=1975, iq=0, schedule=[]}
                  Mother: Woman{name='Alice', surname='Smith', year=1980, iq=0, schedule=[]}
                  Children: [Human{name='Ryan', surname='Smith', year=2004, iq=0, schedule=[]}]
                  Pets: []
                  Total Persons in Family: 3
                }]""";

        String expectedOutput = isBiggerThan ? isBiggerThanString.trim() : isLessThanString.trim();

        String actualOutput = outContent.toString().trim();

        Assert.assertEquals(expectedOutput, actualOutput);
    }

    @Test
    void testCountFamiliesWithMemberNumber() {
        family1.addChild(new Human("Bob", "Doe", 1995));
        family1.addChild(new Human("Davida", "Doe", 2000, family1));

        Human child_family2 = new Human("Ryan", "Smith", 2004, family2);
        family2.addChild(child_family2);

        Family family3 = new Family(new Man("Charlie", "Brown", 1982), new Woman("Lucy", "Brown", 1987));
        family3.addChild(new Human("Sally", "Brown", 2005, family3));
        family3.addChild(new Human("Linus", "Brown", 2008, family3));
        family3.setPets(new Dog("Snoopy", 3));

        // Add families to the mocked FamilyDao
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);
        mockFamilyDao.saveFamily(family3);

        // Call countFamiliesWithMemberNumber() method for different numbers
        int count1 = familyService.countFamiliesWithMemberNumber(3);
        int count2 = familyService.countFamiliesWithMemberNumber(4);
        int count3 = familyService.countFamiliesWithMemberNumber(5);

        // Verify the counts
        Assert.assertEquals(count2, 2); // family1 and family3 have 4 members
        Assert.assertEquals(count1, 1); // family2 has 3 members
        Assert.assertEquals(count3, 0); // No family has 5 members
    }

    @Test
    void testCreateNewFamily() {
        Man father = new Man("John", "Doe", 1980);
        Woman mother = new Woman("Jane", "Doe", 1985);

        // Call createNewFamily() method
        familyService.createNewFamily(father, mother);

        // Get all families from the mocked FamilyDao
        int totalFamilies = familyService.getAllFamilies().size();

        // Verify that a new family is created and added to the list
        Assert.assertEquals(totalFamilies, 1);

        // Verify that the created family has the correct members
        Family createdFamily = familyService.getAllFamilies().get(0);
        Assert.assertEquals(father, createdFamily.getFather());
        Assert.assertEquals(mother, createdFamily.getMother());
        Assert.assertTrue(createdFamily.getChildren().isEmpty());
        Assert.assertTrue(createdFamily.getPets().isEmpty());
    }

    @Test
    void testDeleteFamilyByIndexVoid() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteFamilyByIndex() method
        familyService.deleteFamilyByIndex(0);

        // Verify the result and printed output
        Assert.assertEquals(familyService.getAllFamilies().size(), 1);
        Assert.assertEquals(family2, familyService.getAllFamilies().get(0));

        String expectedOutput = ""; // Since the family is successfully deleted, no message should be printed
        Assert.assertEquals(outContent.toString().trim(), expectedOutput);
    }

    @Test
    void testDeleteFamilyByInvalidIndexVoid() {
        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteFamilyByIndex() method with an invalid index
        familyService.deleteFamilyByIndex(2);

        // Verify the printed output for an invalid index
        String expectedOutput = "The list of families is empty.\n" +
                "There's no 2 in the family list.";
        Assert.assertEquals(outContent.toString().trim().replaceAll("\\s", ""), expectedOutput.trim().replaceAll("\\s", ""));
    }

    @Test
    void testAdoptChild() {
        Human child = new Human("Adopted", "Child", 2010);

        // Save families to the mock FamilyDao
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call adoptChild() method
        Family adoptedFamily = familyService.adoptChild(family1, child);

        // Verify the result
        Assert.assertNotNull(adoptedFamily);
        Assert.assertTrue(adoptedFamily.getChildren().contains(child));
        Assert.assertEquals(family1, adoptedFamily);

        // Additional test for child adoption failure
        Family nonAdoptedFamily = familyService.adoptChild(family2, child);
        Assert.assertNull(nonAdoptedFamily);
    }

    @Test
    void testDeleteAllChildrenOlderThan() {
        Human olderChild = new Human("Older", "Child", 1990);
        Human youngerChild = new Human("Younger", "Child", 2005);

        // Add children to the family
        family1.addChild(olderChild);
        family1.addChild(youngerChild);

        // Save the family to the mock FamilyDao
        mockFamilyDao.saveFamily(family1);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteAllChildrenOlderThan() method with age 20
        familyService.deleteAllChildrenOlderThan(20);

        // Verify the result and printed output
        Assert.assertEquals(family1.getChildren().size(), 1);
        Assert.assertFalse(family1.getChildren().contains(olderChild));
        Assert.assertTrue(family1.getChildren().contains(youngerChild));

        // Reset System.out to the original
        System.setOut(originalOut);
    }

    @Test
    void testCount() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call the count() method
        int count = familyService.count();

        // Verify the result
        Assert.assertEquals(count, 2);
    }

    @Test
    void testGetFamilyById() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call the getFamilyById() method with a valid index
        Family retrievedFamily = familyService.getFamilyById(0);

        // Verify the result
        Assert.assertNotNull(retrievedFamily);
        Assert.assertEquals(retrievedFamily, family1);
    }

    @Test
    void testGetFamilyByIdWithInvalidIndex() {
        // Call the getFamilyById() method with an invalid index
        Family retrievedFamily = familyService.getFamilyById(5);

        // Verify the result
        Assert.assertNull(retrievedFamily);
        // You may also want to check if the expected error message is printed to System.out
    }

    @Test
    void testGetPets() {
        Dog pet1 = new Dog("Buddy", 4);
        DomesticCat pet2 = new DomesticCat("Whiskers", 3);

        family1.setPets(pet1);
        family2.setPets(pet2);

        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call the getPets() method with valid indices
        List<Pet> petsFromFamily1 = familyService.getPets(0);
        List<Pet> petsFromFamily2 = familyService.getPets(1);

        // Verify the results
        Assert.assertNotNull(petsFromFamily1);
        Assert.assertEquals(petsFromFamily1.size(), 1);
        Assert.assertTrue(petsFromFamily1.contains(pet1));

        Assert.assertNotNull(petsFromFamily2);
        Assert.assertEquals(petsFromFamily2.size(), 1);
        Assert.assertTrue(petsFromFamily2.contains(pet2));
    }

    @Test
    void testGetPetsWithInvalidIndex() {
        // Call the getPets() method with an invalid index
        List<Pet> pets = familyService.getPets(5);

        Assert.assertNotNull(pets);
        Assert.assertTrue(pets.isEmpty());
    }

    @Test
    void testAddPet() {
        mockFamilyDao.saveFamily(family1);

        Dog pet = new Dog("Buddy", 4);

        // Call the addPet() method
        familyService.addPet(0, pet);

        // Verify the result
        Family updatedFamily = familyService.getFamilyByIndex(0);
        Assert.assertNotNull(updatedFamily);
        Set<Pet> pets = updatedFamily.getPets();

        Assert.assertNotNull(pets);
        Assert.assertEquals(pets.size(), 1);
        Assert.assertTrue(pets.contains(pet));
    }

    @Test
    void testAddPetWithInvalidIndex() {
        DomesticCat pet = new DomesticCat("Whiskers", 3);

        // Call the addPet() method with an invalid index
        familyService.addPet(5, pet);
    }

    @Test
    void testAddPetWithNullFamily() {
        Fish pet = new Fish("Tweety", 3);

        // Call the addPet() method with null family
        familyService.addPet(0, pet);
    }

    String randomMaleName = FamilyService.getRandomMaleName();
    String randomFemaleName = FamilyService.getRandomFemaleName();

    @Test
    void testBornChildWithExistingFamily() {
        mockFamilyDao.saveFamily(family1);
        family1.getFather().setIQ(125);
        family1.getMother().setIQ(105);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the bornChild() method
        Family updatedFamily = familyService.bornChild(family1, randomMaleName, randomFemaleName);

        // Verify the result
        Assert.assertNotNull(updatedFamily);
        Assert.assertEquals(updatedFamily.getChildren().size(), 1);

        Human child = updatedFamily.getChildren().get(0);
        Assert.assertNotNull(child);
        Assert.assertNotNull(child.getName());
        Assert.assertFalse(child.getName().isEmpty());
        Assert.assertEquals("Doe", child.getSurname());
        Assert.assertEquals(updatedFamily, child.getFamily());
        int inheritedIQ = familyService.calculateInheritedIQ(updatedFamily.getFather(), updatedFamily.getMother(), child);
        Assert.assertEquals(child.getIQ(), inheritedIQ);
        Assert.assertTrue(child instanceof Man || child instanceof Woman);

        // Validate the printed output
        Assert.assertFalse(outContent.toString().contains("Creating a new family for the child."));

        // Reset System.out to the original
        System.setOut(originalOut);
    }

    @Test
    public void testBornChildWithCustomNames() {
        mockFamilyDao.saveFamily(family1);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the bornChild() method with custom names
        Family updatedFamily = familyService.bornChild(family1, "John Jr.", "Jane Jr.");

        // Verify the result
        Assert.assertNotNull(updatedFamily);
        Assert.assertEquals(updatedFamily.getChildren().size(), 1);

        Human child = updatedFamily.getChildren().get(0);
        Assert.assertNotNull(child);
        Assert.assertTrue(child.getName().equals("John Jr.") || child.getName().equals("Jane Jr."));

        // Validate the printed output
        Assert.assertFalse(outContent.toString().contains("Creating a new family for the child."));

        // Reset System.out to the original
        System.setOut(originalOut);
    }
}