package homework8Test;

import org.homework8.Family;
import org.homework8.Man;
import org.homework8.Woman;

public class TestUtilities {

    public static Family setUpFamily(String fatherName, String fatherSurname, int fatherBirthYear,
                                     String motherName, String motherSurname, int motherBirthYear) {
        Man father = new Man(fatherName, fatherSurname, fatherBirthYear);
        Woman mother = new Woman(motherName, motherSurname, motherBirthYear);
        return new Family(father, mother);
    }

    public static Family setUpFamily1() {
        return setUpFamily("John", "Doe", 1970, "Jane", "Doe", 1975);
    }

    public static Family setUpFamily2() {
        return setUpFamily("Bob", "Smith", 1975, "Alice", "Smith", 1980);
    }
}
