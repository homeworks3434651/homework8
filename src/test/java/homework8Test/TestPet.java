package homework8Test;

import org.homework8.Pet;
import org.homework8.Species;

import java.util.TreeSet;

public class TestPet extends Pet {
    public TestPet(String nickname, int age) {
        super(nickname, age);
        super.setTrickLevel(0);
        super.setHabits(new TreeSet<>(super.getHabits()));
        setSpecies(Species.UNKNOWN);
    }

    @Override
    public void respond() {
        // Implement abstract method
    }

    @Override
    public void eat() {
        // Implement abstract method
    }
}
