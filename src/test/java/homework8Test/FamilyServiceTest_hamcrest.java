package homework8Test;

import org.homework8.*;
import org.junit.jupiter.api.*;
import java.io.*;
import java.util.*;

import org.hamcrest.MatcherAssert;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class FamilyServiceTest_hamcrest {

    private CollectionFamilyDao mockFamilyDao;
    private FamilyService familyService;
    private PrintStream originalOut;

    @BeforeEach
    void setUp() {
        // Save the original System.out before redirecting
        originalOut = System.out;

        // Create a familyService with a mocked FamilyDao
        mockFamilyDao = new CollectionFamilyDao();
        familyService = new FamilyService(mockFamilyDao);
    }

    @AfterEach
    void tearDown() {
        System.setOut(originalOut);
    }

    Family family1 = TestUtilities.setUpFamily1();
    Family family2 = TestUtilities.setUpFamily2();

    @Test
    void testGetAllFamilies() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call getAllFamilies() method
        List<Family> allFamilies = familyService.getAllFamilies();

        // Verify the result
        assertThat(allFamilies, hasSize(2));
        assertThat(allFamilies, containsInAnyOrder(family1, family2));
    }

    @Test
    void testGetFamilyByIndex() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call getFamilyByIndex() method
        Family retrievedFamily = familyService.getFamilyByIndex(0);

        // Verify the result
        assertThat(retrievedFamily, is(notNullValue()));
        assertThat(retrievedFamily, is(equalTo(family1)));
    }

    @Test
    void testDeleteFamily() {
        mockFamilyDao.saveFamily(family1);

        // Call deleteFamily() method
        boolean deleted = familyService.deleteFamily(family1);

        // Verify the result
        assertThat(deleted, is(true));
        assertThat(familyService.getAllFamilies(), hasSize(0));
    }

    @Test
    void testDeleteNonExistingFamily() {
        // Call deleteFamily() method
        boolean deleted = familyService.deleteFamily(family1);

        // Verify the result
        assertThat(deleted, is(false));
        assertThat(familyService.getAllFamilies(), hasSize(0));
    }

    @Test
    void testDeleteFamilyByIndex() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call deleteFamilyByIndex() method
        assertThat(familyService.deleteFamily(0), is(true));

        // Verify the result
        assertThat(familyService.getAllFamilies(), hasSize(1));
        assertThat(familyService.getAllFamilies(), not(contains(family1)));
        assertThat(familyService.getAllFamilies(), contains(family2));
    }

    @Test
    void testDeleteNonExistingFamilyByIndex() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call deleteFamilyByIndex() with a non-existing index
        assertThat(familyService.deleteFamily(2), is(false));

        // Verify the result (no change in families)
        assertThat(familyService.getAllFamilies(), hasSize(2));
        assertThat(familyService.getAllFamilies(), containsInAnyOrder(family1, family2));
    }

    @Test
    void testDeleteFamilyByNegativeIndex() {
        mockFamilyDao.saveFamily(family1);

        // Call deleteFamilyByIndex() with a negative index
        assertThat(familyService.deleteFamily(-1), is(false));

        // Verify the result (no change in families)
        assertThat(familyService.getAllFamilies(), hasSize(1));
        assertThat(familyService.getAllFamilies(), contains(family1));
    }

    @Test
    void testSaveNewFamily() {
        familyService.saveFamily(family1);

        // Verify the result
        assertThat(familyService.getAllFamilies(), hasSize(1));
        assertThat(familyService.getAllFamilies(), contains(family1));
    }

    @Test
    void testUpdateExistingFamily() {
        mockFamilyDao.saveFamily(family1);

        // Update the existing family with the same members
        family1.getFather().setYear(1980);
        family1.getMother().setYear(1985);

        // Call saveFamily() method with the updated family
        familyService.saveFamily(family1);

        // Verify the result (existing family should be updated, not added)
        assertThat(familyService.getAllFamilies(), hasSize(1));
        assertThat(familyService.getAllFamilies(), contains(family1));
    }

    @Test
    void testSaveNullFamily() {
        // Call saveFamily() method with null family
        familyService.saveFamily(null);

        // Verify the result (no change in families)
        assertThat(familyService.getAllFamilies(), is(empty()));
    }

    @Test
    void testDisplayAllFamilies() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Verify the printed output
        String expectedOutput = String.format(
                "%nDisplaying all families:%n%s%n%s%n%n",
                family1, family2
        );

        // Test displayAllFamilies with families
        testDisplayAllFamiliesWithOutput(expectedOutput);
    }

    void testDisplayAllFamiliesEmptyList() {
        // Call displayAllFamilies() method with an empty list
        familyService.displayAllFamilies();

        // Verify the printed output using Hamcrest
        String expectedOutput = String.format("Displaying all families:%nThe list of families is empty.").trim();

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call displayAllFamilies() method
        familyService.displayAllFamilies();

        // Verify the printed output using Hamcrest
        assertThat(outContent.toString().trim().replaceAll("\\s", ""), equalTo(expectedOutput.trim().replaceAll("\\s", "")));

        // Reset System.out
        System.setOut(originalOut);
    }

    private void testDisplayAllFamiliesWithOutput(String expectedOutput) {
        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call displayAllFamilies() method
        familyService.displayAllFamilies();

        // Verify the printed output
        assertThat(outContent.toString(), is(expectedOutput));

        // Reset System.out
        System.setOut(originalOut);
    }

    @Test
    void testGetFamiliesBiggerThan() {
        testFamilyFilteringMethod(3, true);
    }

    @Test
    void testGetFamiliesLessThan() {
        testFamilyFilteringMethod(4, false);
    }

    private void testFamilyFilteringMethod(int numberOfMembers, boolean isBiggerThan) {
        family1.addChild(new Human("Bob", "Doe", 1995));
        family1.addChild(new Human("Davida", "Doe", 2000, family1));
        mockFamilyDao.saveFamily(family1);

        Human child_family2 = new Human("Ryan", "Smith", 2004, family2);
        family2.addChild(child_family2);
        mockFamilyDao.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the appropriate family filtering method
        List<Family> resultFamilies = isBiggerThan ?
                familyService.getFamiliesBiggerThan(numberOfMembers) :
                familyService.getFamiliesLessThan(numberOfMembers);

        // Verify the result and printed output using Hamcrest assertions
        MatcherAssert.assertThat(resultFamilies, hasSize(1));
        MatcherAssert.assertThat(resultFamilies, contains(isBiggerThan ? family1 : family2));

        String isBiggerThanString = """
        [Family{
          Father: Man{name='John', surname='Doe', year=1970, iq=0, schedule=[]}
          Mother: Woman{name='Jane', surname='Doe', year=1975, iq=0, schedule=[]}
          Children: [Human{name='Bob', surname='Doe', year=1995, iq=0, schedule=[]}, Human{name='Davida', surname='Doe', year=2000, iq=0, schedule=[]}]
          Pets: []
          Total Persons in Family: 4
        }]""";

        String isLessThanString = """
        [Family{
          Father: Man{name='Bob', surname='Smith', year=1975, iq=0, schedule=[]}
          Mother: Woman{name='Alice', surname='Smith', year=1980, iq=0, schedule=[]}
          Children: [Human{name='Ryan', surname='Smith', year=2004, iq=0, schedule=[]}]
          Pets: []
          Total Persons in Family: 3
        }]""";

        String expectedOutput = isBiggerThan ? isBiggerThanString.trim() : isLessThanString.trim();

        String actualOutput = outContent.toString().trim();

        MatcherAssert.assertThat(actualOutput, equalTo(expectedOutput));
    }

    @Test
    void testCountFamiliesWithMemberNumber() {
        family1.addChild(new Human("Bob", "Doe", 1995));
        family1.addChild(new Human("Davida", "Doe", 2000, family1));

        Human child_family2 = new Human("Ryan", "Smith", 2004, family2);
        family2.addChild(child_family2);

        Family family3 = new Family(new Man("Charlie", "Brown", 1982), new Woman("Lucy", "Brown", 1987));
        family3.addChild(new Human("Sally", "Brown", 2005, family3));
        family3.addChild(new Human("Linus", "Brown", 2008, family3));
        family3.setPets(new Dog("Snoopy", 3));

        // Add families to the mocked FamilyDao
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);
        mockFamilyDao.saveFamily(family3);

        // Call countFamiliesWithMemberNumber() method for different numbers
        int count1 = familyService.countFamiliesWithMemberNumber(3);
        int count2 = familyService.countFamiliesWithMemberNumber(4);
        int count3 = familyService.countFamiliesWithMemberNumber(5);

        // Verify the counts using Hamcrest assertions
        assertThat(count2, is(2)); // family1 and family3 have 4 members
        assertThat(count1, is(1)); // family2 has 3 members
        assertThat(count3, is(0)); // No family has 5 members
    }

    @Test
    void testCreateNewFamily() {
        Man father = new Man("John", "Doe", 1980);
        Woman mother = new Woman("Jane", "Doe", 1985);

        // Call createNewFamily() method
        familyService.createNewFamily(father, mother);

        // Get all families from the mocked FamilyDao
        int totalFamilies = familyService.getAllFamilies().size();

        // Verify that a new family is created and added to the list
        assertThat(totalFamilies, is(1));

        // Verify that the created family has the correct members using Hamcrest assertions
        Family createdFamily = familyService.getAllFamilies().get(0);
        assertThat(createdFamily.getFather(), is(equalTo(father)));
        assertThat(createdFamily.getMother(), is(equalTo(mother)));
        assertThat(createdFamily.getChildren(), is(empty()));
        assertThat(createdFamily.getPets(), is(empty()));
    }

    @Test
    void testDeleteFamilyByIndexVoid() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteFamilyByIndex() method
        familyService.deleteFamilyByIndex(0);

        // Verify the result and printed output using Hamcrest assertions
        assertThat(familyService.getAllFamilies(), hasSize(1));
        assertThat(familyService.getAllFamilies(), contains(family2));

        String expectedOutput = ""; // Since the family is successfully deleted, no message should be printed
        assertThat(outContent.toString().trim(), is(expectedOutput));
    }

    @Test
    void testDeleteFamilyByInvalidIndexVoid() {
        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteFamilyByIndex() method with an invalid index
        familyService.deleteFamilyByIndex(2);

        // Verify the printed output for an invalid index using Hamcrest assertions
        String expectedOutput = "The list of families is empty.\n" +
                "There's no 2 in the family list.";
        assertThat(outContent.toString().trim().replaceAll("\\s", ""), is(expectedOutput.trim().replaceAll("\\s", "")));
    }

    @Test
    void testAdoptChild() {
        Human child = new Human("Adopted", "Child", 2010);

        // Save families to the mock FamilyDao
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call adoptChild() method
        Family adoptedFamily = familyService.adoptChild(family1, child);

        // Verify the result and printed output
        assertThat(adoptedFamily, is(notNullValue()));
        assertThat(adoptedFamily.getChildren(), hasItem(child));
        assertThat(adoptedFamily, is(equalTo(family1)));
        assertThat(outContent.toString(), not(containsString("Child adoption failed.")));

        familyService.adoptChild(family2, child);
        assertThat(outContent.toString(), containsString("Child adoption failed."));

        // Reset System.out to the original
        System.setOut(originalOut);
    }

    @Test
    void testDeleteAllChildrenOlderThan() {
        Human olderChild = new Human("Older", "Child", 1990);
        Human youngerChild = new Human("Younger", "Child", 2005);

        // Add children to the family
        family1.addChild(olderChild);
        family1.addChild(youngerChild);

        // Save the family to the mock FamilyDao
        mockFamilyDao.saveFamily(family1);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteAllChildrenOlderThan() method with age 20
        familyService.deleteAllChildrenOlderThan(20);

        // Verify the result and printed output
        assertThat(family1.getChildren(), hasSize(1));
        assertThat(family1.getChildren(), not(contains(olderChild)));
        assertThat(family1.getChildren(), contains(youngerChild));

        // Reset System.out to the original
        System.setOut(originalOut);
    }

    @Test
    void testCount() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call the count() method
        int count = familyService.count();

        // Verify the result
        assertThat(count, is(equalTo(2)));
    }

    @Test
    void testGetFamilyById() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call the getFamilyById() method with a valid index
        Family retrievedFamily = familyService.getFamilyById(0);

        // Verify the result
        assertThat(retrievedFamily, is(notNullValue()));
        assertThat(retrievedFamily, is(equalTo(family1)));
    }

    @Test
    void testGetFamilyByIdWithInvalidIndex() {
        // Call the getFamilyById() method with an invalid index
        Family retrievedFamily = familyService.getFamilyById(5);

        // Verify the result
        assertThat(retrievedFamily, is(nullValue()));
        // You may also want to check if the expected error message is printed to System.out
    }

    @Test
    void testGetPets() {
        Dog pet1 = new Dog("Buddy", 4);
        DomesticCat pet2 = new DomesticCat("Whiskers", 3);

        family1.setPets(pet1);
        family2.setPets(pet2);

        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call the getPets() method with valid indices
        List<Pet> petsFromFamily1 = familyService.getPets(0);
        List<Pet> petsFromFamily2 = familyService.getPets(1);

        // Verify the results
        assertThat(petsFromFamily1, is(notNullValue()));
        assertThat(petsFromFamily1, hasSize(1));
        assertThat(petsFromFamily1, contains(pet1));

        assertThat(petsFromFamily2, is(notNullValue()));
        assertThat(petsFromFamily2, hasSize(1));
        assertThat(petsFromFamily2, contains(pet2));
    }

    @Test
    void testGetPetsWithInvalidIndex() {
        // Call the getPets() method with an invalid index
        List<Pet> pets = familyService.getPets(5);

        assertThat(pets, is(notNullValue()));
        assertThat(pets, empty());
    }

    @Test
    void testAddPet() {
        mockFamilyDao.saveFamily(family1);

        Dog pet = new Dog("Buddy", 4);

        // Call the addPet() method
        familyService.addPet(0, pet);

        // Verify the result
        Family updatedFamily = familyService.getFamilyByIndex(0);
        assertThat(updatedFamily, is(notNullValue()));
        Set<Pet> pets = updatedFamily.getPets();

        assertThat(pets, is(notNullValue()));
        assertThat(pets, hasSize(1));
        assertThat(pets, contains(pet));
    }

    @Test
    void testAddPetWithInvalidIndex() {
        DomesticCat pet = new DomesticCat("Whiskers", 3);

        // Call the addPet() method with an invalid index
        familyService.addPet(5, pet);
    }

    @Test
    void testAddPetWithNullFamily() {
        Fish pet = new Fish("Tweety", 3);

        // Call the addPet() method with null family
        familyService.addPet(0, pet);
    }

    String randomMaleName = familyService.getRandomMaleName();
    String randomFemaleName = familyService.getRandomFemaleName();

    @Test
    void testBornChildWithExistingFamily() {
        mockFamilyDao.saveFamily(family1);
        family1.getFather().setIQ(125);
        family1.getMother().setIQ(105);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the bornChild() method
        Family updatedFamily = familyService.bornChild(family1, randomMaleName, randomFemaleName);

        // Verify the result
        assertThat(updatedFamily, is(notNullValue()));
        assertThat(updatedFamily.getChildren(), hasSize(1));

        Human child = updatedFamily.getChildren().get(0);
        assertThat(child, is(notNullValue()));
        assertThat(child.getName(), is(not(emptyString())));
        assertThat(child.getSurname(), is("Doe"));
        assertThat(child.getFamily(), is(family1));
        int inheritedIQ = familyService.calculateInheritedIQ(family1.getFather(), family1.getMother(), child);
        assertThat(child.getIQ(), is(inheritedIQ));
        assertThat(child, either(instanceOf(Man.class)).or(instanceOf(Woman.class)));

        // Validate the printed output
        assertThat(outContent.toString(), not(containsString("Creating a new family for the child.")));

        // Reset System.out to the original
        System.setOut(originalOut);
    }

    @Test
    void testBornChildWithCustomNames() {
        mockFamilyDao.saveFamily(family1);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the bornChild() method with custom names
        Family updatedFamily = familyService.bornChild(family1, "John Jr.", "Jane Jr.");

        // Verify the result
        assertThat(updatedFamily, is(notNullValue()));
        assertThat(updatedFamily.getChildren(), hasSize(1));

        Human child = updatedFamily.getChildren().get(0);
        assertThat(child, is(notNullValue()));
        assertThat(child.getName(), anyOf(equalTo("John Jr."), equalTo("Jane Jr.")));

        // Validate the printed output
        assertThat(outContent.toString(), not(containsString("Creating a new family for the child.")));

        // Reset System.out to the original
        System.setOut(originalOut);
    }
}
