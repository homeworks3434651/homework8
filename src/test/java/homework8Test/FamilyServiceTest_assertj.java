package homework8Test;

import org.homework8.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;

import java.io.*;
import java.util.*;

public class FamilyServiceTest_assertj {

    private CollectionFamilyDao mockFamilyDao;
    private FamilyService familyService;
    private PrintStream originalOut;

    @BeforeEach
    void setUp() {
        // Save the original System.out before redirecting
        originalOut = System.out;

        // Create a familyService with a mocked FamilyDao
        mockFamilyDao = new CollectionFamilyDao();
        familyService = new FamilyService(mockFamilyDao);
    }

    @AfterEach
    void tearDown() {
        System.setOut(originalOut);
    }

    Family family1 = TestUtilities.setUpFamily1();
    Family family2 = TestUtilities.setUpFamily2();

    @Test
    void testGetAllFamilies() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call getAllFamilies() method
        List<Family> allFamilies = familyService.getAllFamilies();

        // Using AssertJ for assertions
        Assertions.assertThat(allFamilies).hasSize(2)
                .containsExactlyInAnyOrder(family1, family2);
    }

    @Test
    void testGetFamilyByIndex() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call getFamilyByIndex() method
        Family retrievedFamily = familyService.getFamilyByIndex(0);

        // Using AssertJ for assertions
        Assertions.assertThat(retrievedFamily).isEqualTo(family1);
    }

    @Test
    void testDeleteFamily() {
        mockFamilyDao.saveFamily(family1);

        // Call deleteFamily() method
        boolean deleted = familyService.deleteFamily(family1);

        // Using AssertJ for assertions
        Assertions.assertThat(deleted).isTrue();
        Assertions.assertThat(familyService.getAllFamilies()).isEmpty();
    }

    @Test
    void testDeleteNonExistingFamily() {
        // Call deleteFamily() method
        boolean deleted = familyService.deleteFamily(family1);

        // Using AssertJ for assertions
        Assertions.assertThat(deleted).isFalse();
        Assertions.assertThat(familyService.getAllFamilies()).isEmpty();
    }

    @Test
    void testDeleteFamilyByIndex() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call deleteFamilyByIndex() method
        boolean deleted = familyService.deleteFamily(0);

        // Using AssertJ for assertions
        Assertions.assertThat(deleted).isTrue();
        Assertions.assertThat(familyService.getAllFamilies()).hasSize(1)
                .doesNotContain(family1)
                .contains(family2);
    }

    @Test
    void testDeleteNonExistingFamilyByIndex() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call deleteFamilyByIndex() with a non-existing index
        boolean deleted = familyService.deleteFamily(2);

        // Verify the result (no change in families)
        Assertions.assertThat(deleted).isFalse();
        Assertions.assertThat(familyService.getAllFamilies()).hasSize(2)
                .containsExactlyInAnyOrder(family1, family2);
    }

    @Test
    void testDeleteFamilyByNegativeIndex() {
        mockFamilyDao.saveFamily(family1);

        // Call deleteFamilyByIndex() with a negative index
        boolean deleted = familyService.deleteFamily(-1);

        // Verify the result (no change in families)
        Assertions.assertThat(deleted).isFalse();
        Assertions.assertThat(familyService.getAllFamilies()).hasSize(1)
                .containsExactly(family1);
    }

    @Test
    void testSaveNewFamily() {
        familyService.saveFamily(family1);

        // Verify the result
        Assertions.assertThat(familyService.getAllFamilies()).hasSize(1)
                .containsExactly(family1);
    }

    @Test
    void testUpdateExistingFamily() {
        mockFamilyDao.saveFamily(family1);

        // Update the existing family with the same members
        family1.getFather().setYear(1980);
        family1.getMother().setYear(1985);

        // Call saveFamily() method with the updated family
        familyService.saveFamily(family1);

        // Verify the result (existing family should be updated, not added)
        Assertions.assertThat(familyService.getAllFamilies()).hasSize(1)
                .containsExactly(family1);
    }

    @Test
    void testSaveNullFamily() {
        // Call saveFamily() method with null family
        familyService.saveFamily(null);

        // Verify the result (no change in families)
        Assertions.assertThat(familyService.getAllFamilies()).isEmpty();
    }

    @Test
    void testDisplayAllFamilies() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Verify the printed output
        String expectedOutput = String.format(
                "%nDisplaying all families:%n%s%n%s%n%n",
                family1, family2
        );

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call displayAllFamilies() method
        familyService.displayAllFamilies();

        // Verify the printed output using AssertJ
        Assertions.assertThat(outContent.toString()).isEqualTo(expectedOutput);

        // Reset System.out
        System.setOut(originalOut);
    }

    @Test
    void testDisplayAllFamiliesEmptyList() {
        // Call displayAllFamilies() method with an empty list
        familyService.displayAllFamilies();

        // Verify the printed output using AssertJ
        String expectedOutput = String.format("Displaying all families:%nThe list of families is empty.").trim();

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call displayAllFamilies() method
        familyService.displayAllFamilies();

        // Verify the printed output using AssertJ
        Assertions.assertThat(outContent.toString().trim().replaceAll("\\s", "")).isEqualTo(expectedOutput.trim().replaceAll("\\s", ""));

        // Reset System.out
        System.setOut(originalOut);
    }

    @Test
    void testGetFamiliesBiggerThan() {
        testFamilyFilteringMethod(3, true);
    }

    @Test
    void testGetFamiliesLessThan() {
        testFamilyFilteringMethod(4, false);
    }

    private void testFamilyFilteringMethod(int numberOfMembers, boolean isBiggerThan) {
        family1.addChild(new Human("Bob", "Doe", 1995));
        family1.addChild(new Human("Davida", "Doe", 2000, family1));
        mockFamilyDao.saveFamily(family1);

        Human child_family2 = new Human("Ryan", "Smith", 2004, family2);
        family2.addChild(child_family2);
        mockFamilyDao.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the appropriate family filtering method
        List<Family> resultFamilies = isBiggerThan ?
                familyService.getFamiliesBiggerThan(numberOfMembers) :
                familyService.getFamiliesLessThan(numberOfMembers);

        // Using AssertJ for assertions
        Assertions.assertThat(resultFamilies).hasSize(1)
                .containsExactly(isBiggerThan ? family1 : family2);

        String isBiggerThanString = """
                [Family{
                  Father: Man{name='John', surname='Doe', year=1970, iq=0, schedule=[]}
                  Mother: Woman{name='Jane', surname='Doe', year=1975, iq=0, schedule=[]}
                  Children: [Human{name='Bob', surname='Doe', year=1995, iq=0, schedule=[]}, Human{name='Davida', surname='Doe', year=2000, iq=0, schedule=[]}]
                  Pets: []
                  Total Persons in Family: 4
                }]""";

        String isLessThanString = """
                [Family{
                  Father: Man{name='Bob', surname='Smith', year=1975, iq=0, schedule=[]}
                  Mother: Woman{name='Alice', surname='Smith', year=1980, iq=0, schedule=[]}
                  Children: [Human{name='Ryan', surname='Smith', year=2004, iq=0, schedule=[]}]
                  Pets: []
                  Total Persons in Family: 3
                }]""";

        String expectedOutput = isBiggerThan ? isBiggerThanString.trim() : isLessThanString.trim();

        // Verify the printed output using AssertJ
        Assertions.assertThat(outContent.toString().trim()).isEqualTo(expectedOutput);
    }

    @Test
    void testCountFamiliesWithMemberNumber() {
        family1.addChild(new Human("Bob", "Doe", 1995));
        family1.addChild(new Human("Davida", "Doe", 2000, family1));

        Human child_family2 = new Human("Ryan", "Smith", 2004, family2);
        family2.addChild(child_family2);

        Family family3 = new Family(new Man("Charlie", "Brown", 1982), new Woman("Lucy", "Brown", 1987));
        family3.addChild(new Human("Sally", "Brown", 2005, family3));
        family3.addChild(new Human("Linus", "Brown", 2008, family3));
        family3.setPets(new Dog("Snoopy", 3));

        // Add families to the mocked FamilyDao
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);
        mockFamilyDao.saveFamily(family3);

        // Call countFamiliesWithMemberNumber() method for different numbers
        int count1 = familyService.countFamiliesWithMemberNumber(3);
        int count2 = familyService.countFamiliesWithMemberNumber(4);
        int count3 = familyService.countFamiliesWithMemberNumber(5);

        // Using AssertJ for assertions
        Assertions.assertThat(count2).isEqualTo(2); // family1 and family3 have 4 members
        Assertions.assertThat(count1).isEqualTo(1); // family2 has 3 members
        Assertions.assertThat(count3).isEqualTo(0); // No family has 5 members
    }

    @Test
    void testCreateNewFamily() {
        Man father = new Man("John", "Doe", 1980);
        Woman mother = new Woman("Jane", "Doe", 1985);

        // Call createNewFamily() method
        familyService.createNewFamily(father, mother);

        // Get all families from the mocked FamilyDao
        List<Family> allFamilies = familyService.getAllFamilies();

        // Using AssertJ for assertions
        Assertions.assertThat(allFamilies).hasSize(1);

        // Verify that the created family has the correct members
        Family createdFamily = allFamilies.get(0);
        Assertions.assertThat(createdFamily.getFather()).isEqualTo(father);
        Assertions.assertThat(createdFamily.getMother()).isEqualTo(mother);
        Assertions.assertThat(createdFamily.getChildren()).isEmpty();
        Assertions.assertThat(createdFamily.getPets()).isEmpty();
    }

    @Test
    void testDeleteFamilyByIndexVoid() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteFamilyByIndex() method
        familyService.deleteFamilyByIndex(0);

        // Using AssertJ for assertions
        Assertions.assertThat(familyService.getAllFamilies()).hasSize(1)
                .containsExactly(family2);

        // Verify the printed output using AssertJ
        Assertions.assertThat(outContent.toString().trim()).isEmpty();
    }

    @Test
    void testDeleteFamilyByInvalidIndexVoid() {
        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteFamilyByIndex() method with an invalid index
        familyService.deleteFamilyByIndex(2);
        String expectedOutput = "The list of families is empty.\n" +
                "There's no 2 in the family list.";

        Assertions.assertThat(outContent.toString().trim().replaceAll("\\s", "")).isEqualTo(expectedOutput.trim().replaceAll("\\s", ""));
    }

    @Test
    void testAdoptChild() {
        Human child = new Human("Adopted", "Child", 2010);

        // Save families to the mock FamilyDao
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call adoptChild() method
        Family adoptedFamily = familyService.adoptChild(family1, child);

        // Using AssertJ for assertions
        Assertions.assertThat(adoptedFamily).isNotNull();
        Assertions.assertThat(adoptedFamily.getChildren()).contains(child);
        Assertions.assertThat(adoptedFamily).isEqualTo(family1);

        // Reset System.out to the original
        System.setOut(originalOut);
    }

    @Test
    void testDeleteAllChildrenOlderThan() {
        Human olderChild = new Human("Older", "Child", 1990);
        Human youngerChild = new Human("Younger", "Child", 2005);

        // Add children to the family
        family1.addChild(olderChild);
        family1.addChild(youngerChild);

        // Save the family to the mock FamilyDao
        mockFamilyDao.saveFamily(family1);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call deleteAllChildrenOlderThan() method with age 20
        familyService.deleteAllChildrenOlderThan(20);

        // Using AssertJ for assertions
        Assertions.assertThat(family1.getChildren()).hasSize(1)
                .containsExactly(youngerChild);

        // Reset System.out to the original
        System.setOut(originalOut);
    }

    @Test
    void testCount() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call the count() method
        int count = familyService.count();

        // Using AssertJ for assertions
        Assertions.assertThat(count).isEqualTo(2);
    }

    @Test
    void testGetFamilyById() {
        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call the getFamilyById() method with a valid index
        Family retrievedFamily = familyService.getFamilyById(0);

        // Using AssertJ for assertions
        Assertions.assertThat(retrievedFamily).isNotNull()
                .isEqualTo(family1);
    }

    @Test
    void testGetFamilyByIdWithInvalidIndex() {
        // Call the getFamilyById() method with an invalid index
        Family retrievedFamily = familyService.getFamilyById(5);

        // Using AssertJ for assertions
        Assertions.assertThat(retrievedFamily).isNull();
        // You may also want to check if the expected error message is printed to System.out
    }

    @Test
    void testGetPets() {
        Dog pet1 = new Dog("Buddy", 4);
        DomesticCat pet2 = new DomesticCat("Whiskers", 3);

        family1.setPets(pet1);
        family2.setPets(pet2);

        mockFamilyDao.saveFamily(family1);
        mockFamilyDao.saveFamily(family2);

        // Call the getPets() method with valid indices
        List<Pet> petsFromFamily1 = familyService.getPets(0);
        List<Pet> petsFromFamily2 = familyService.getPets(1);

        // Using AssertJ for assertions
        Assertions.assertThat(petsFromFamily1).isNotNull()
                .hasSize(1)
                .containsExactly(pet1);

        Assertions.assertThat(petsFromFamily2).isNotNull()
                .hasSize(1)
                .containsExactly(pet2);
    }

    @Test
    void testGetPetsWithInvalidIndex() {
        // Call the getPets() method with an invalid index
        List<Pet> pets = familyService.getPets(5);

        // Using AssertJ for assertions
        Assertions.assertThat(pets).isNotNull()
                .isEmpty();
    }

    @Test
    void testAddPet() {
        mockFamilyDao.saveFamily(family1);

        Dog pet = new Dog("Buddy", 4);

        // Call the addPet() method
        familyService.addPet(0, pet);

        // Verify the result
        Family updatedFamily = familyService.getFamilyByIndex(0);
        Assertions.assertThat(updatedFamily).isNotNull();
        Set<Pet> pets = updatedFamily.getPets();

        Assertions.assertThat(pets).isNotNull()
                .hasSize(1)
                .containsExactly(pet);
    }

    @Test
    void testAddPetWithInvalidIndex() {
        DomesticCat pet = new DomesticCat("Whiskers", 3);

        // Call the addPet() method with an invalid index
        familyService.addPet(5, pet);
    }

    @Test
    void testAddPetWithNullFamily() {
        Fish pet = new Fish("Tweety", 3);

        // Call the addPet() method with null family
        familyService.addPet(0, pet);
    }

    String randomMaleName = FamilyService.getRandomMaleName();
    String randomFemaleName = FamilyService.getRandomFemaleName();

    @Test
    void testBornChildWithExistingFamily() {
        mockFamilyDao.saveFamily(family1);
        family1.getFather().setIQ(125);
        family1.getMother().setIQ(105);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the bornChild() method
        Family updatedFamily = familyService.bornChild(family1, randomMaleName, randomFemaleName);

        // Using AssertJ for assertions
        Assertions.assertThat(updatedFamily).isNotNull();
        Assertions.assertThat(updatedFamily.getChildren()).hasSize(1);

        Human child = updatedFamily.getChildren().get(0);
        Assertions.assertThat(child).isNotNull();
        Assertions.assertThat(child.getName()).isNotNull().isNotEmpty();
        Assertions.assertThat(child.getSurname()).isEqualTo("Doe");
        Assertions.assertThat(child.getFamily()).isEqualTo(family1);

        int inheritedIQ = FamilyService.calculateInheritedIQ(family1.getFather(), family1.getMother(), child);
        Assertions.assertThat(child.getIQ()).isEqualTo(inheritedIQ);
        Assertions.assertThat(child).isInstanceOfAny(Man.class, Woman.class);

        // Validate the printed output using AssertJ
        Assertions.assertThat(outContent.toString()).doesNotContain("Creating a new family for the child.");

        // Reset System.out to the original
        System.setOut(originalOut);
    }

    @Test
    void testBornChildWithCustomNames() {
        mockFamilyDao.saveFamily(family1);

        // Redirect System.out to capture printed output
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the bornChild() method with custom names
        Family updatedFamily = familyService.bornChild(family1, "John Jr.", "Jane Jr.");

        // Using AssertJ for assertions
        Assertions.assertThat(updatedFamily).isNotNull();
        Assertions.assertThat(updatedFamily.getChildren()).hasSize(1);

        Human child = updatedFamily.getChildren().get(0);
        Assertions.assertThat(child).isNotNull();
        Assertions.assertThat(child.getName()).isIn("John Jr.", "Jane Jr.");

        // Validate the printed output using AssertJ
        Assertions.assertThat(outContent.toString()).doesNotContain("Creating a new family for the child.");

        // Reset System.out to the original
        System.setOut(originalOut);
    }
}
