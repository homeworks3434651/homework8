package homework8Test;

import org.homework8.*;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class HumanTest {

    Family family = TestUtilities.setUpFamily1();

    @Test
    public void testSetFamily() {
        Human human3 = new Human("Alex", "Doe", 2000);

        assertTrue(human3.setFamily(family));
        assertEquals(family, human3.getFamily());

        Family family2 = new Family(new Man("Bob", "Smith", 1990), new Woman("Alice", "Smith", 1995));

        assertFalse(human3.setFamily(family2));
        assertEquals(family, human3.getFamily()); // Family should remain unchanged
    }

    @Test
    public void testFeedPetWhenTimeToFeed() {
        Dog pet = new Dog("Buddy", 4);
        pet.setTrickLevel(52);

        family.setPets(pet);

        // Test feeding when it's time to feed
        assertTrue(family.getFather().feedPet(true));
    }

    @Test
    public void testFeedPet_shouldReturnTrueOrFalse() {
        DomesticCat cat = new DomesticCat("Whiskers", 3);
        // Assuming setTrickLevel generates a random value between 0 and 100
        cat.setTrickLevel(50);

        family.setPets(cat);

        // Now, instead of expecting a specific result, you can assert that it's either true or false
        assertTrue(Arrays.asList(true, false).contains(family.getFather().feedPet(false)));
    }

    @Test
    public void testFeedPet() {
        // Arrange
        DomesticCat cat = new DomesticCat("Whiskers", 3);
        Family family = new Family(new Man("John", "Doe", 1970), new Woman("Jane", "Doe", 1975));
        family.setPets(cat);
        family.getFather().setFamily(family);

        // Retrieve the specific pet instance from the set
        Pet specificPet = family.getPets().iterator().next();

        // Test when it's not time to feed, and pet trick level is higher
        specificPet.setTrickLevel(100);
        assertTrue(family.getFather().feedPet(false));

        // Test when it's not time to feed, and pet trick level is small
        specificPet.setTrickLevel(0);
        assertFalse(family.getFather().feedPet(false));
    }

    @Test
    public void testGreetPet() {
        Human child = new Human("Alex", "Doe", 2000);
        family.addChild(child);

        DomesticCat cat = new DomesticCat("Whiskers", 3);
        family.setPets(cat);

        assertEquals("Hello, I greet you Whiskers!", family.getMother().greetPet());

        assertEquals("Hello, my favorite friend Whiskers!", family.getFather().greetPet());

        assertEquals("Hello, Whiskers!", child.greetPet());

        // Test greeting when there is no pet
        Family new_family = new Family(new Man("John", "Smith", 1970), new Woman("Jane", "Smith", 1975));
        Human child_new_family = new Human("Teresa", "Smith", 2000);
        new_family.addChild(child_new_family);
        new_family.setPets(null);
        assertEquals("I don't have a pet.", new_family.getMother().greetPet());
        assertEquals("I don't have a pet.", new_family.getFather().greetPet());
        assertEquals("I don't have a pet.", child_new_family.greetPet());
    }

    @Test
    public void testDescribePet() {
        Dog pet = new Dog("Buddy", 0);
        pet.setAge(3);
        pet.setTrickLevel(52);
        family.setPets(pet);

        // Test describing the pet
        String describePet = family.getMother().describePet();
        System.out.println(describePet);
        assertEquals("I have a DOG. It is 3 years old, and it is very cunning.", describePet);

        // Test describing when there is no pet
        Family new_family = new Family(new Man("John", "Smith", 1970), new Woman("Jane", "Smith", 1975));
        Human child_new_family = new Human("Teresa", "Smith", 2000);
        new_family.addChild(child_new_family);
        new_family.setPets(null);
        assertEquals("I don't have a pet.", new_family.getMother().describePet());
        assertEquals("I don't have a pet.", new_family.getFather().describePet());
        assertEquals("I don't have a pet.", child_new_family.describePet());
    }

    @Test
    public void testRepairCar() {
        Man man = new Man("John", "Doe", 1980);

        // Redirect System.out to capture the printed output
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Call the respond() method
        man.repairCar();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the respond() method
        String expectedOutput = "I'm repairing the car.";
        String actualOutput = outputStreamCaptor.toString().trim();  // Remove leading/trailing whitespaces

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testMakeUp() {
        Woman woman = new Woman("Alice", "Smith", 1980);

        // Redirect System.out to capture the printed output
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Call the respond() method
        woman.makeup();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the respond() method
        String expectedOutput = "I'm applying makeup.";
        String actualOutput = outputStreamCaptor.toString().trim();  // Remove leading/trailing whitespaces

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testEquals_and_hashCode() {
        Man human1 = new Man("John", "Doe", 1980);
        Man human2 = new Man("John", "Doe", 1980);
        Man human3 = new Man("Jane", "Doe", 1985);

        assertEquals(human1, human2);
        assertNotEquals(human1, human3);

        assertEquals(human1.hashCode(), human2.hashCode());
        assertNotEquals(human1.hashCode(), human3.hashCode());
    }

    @Test
    public void testToString() {
        Family family = new Family(new Man("Charlie", "Brown", 1990),
                new Woman("Lucy", "Brown", 1992));

        Human human = new Human("Bryan", "Brown", 2010, 120, family,
                new HashMap<>(
                        Map.of(DayOfWeek.TUESDAY, List.of("Studying"),
                                DayOfWeek.SATURDAY, List.of("Gym"))));

        String expectedOutput = "Human{name='Bryan', surname='Brown', year=2010, iq=120, schedule=[[TUESDAY, [Studying]], [SATURDAY, [Gym]]]}";

        assertEquals(expectedOutput, human.toString());

        Man man = new Man("John", "Doe", 1980);
        man.setIQ(120);

        expectedOutput = "Man{name='John', surname='Doe', year=1980, iq=120, schedule=[]}";
        assertEquals(expectedOutput, man.toString());

        Woman woman = new Woman("Alice", "Smith", 1980);
        woman.setSchedule(new HashMap<>(
                Map.of(
                        DayOfWeek.WEDNESDAY, List.of("Club"),
                        DayOfWeek.SATURDAY, List.of("Cafe")
                )
        ));
        woman.addDayToSchedule(DayOfWeek.FRIDAY, "Restaurant");

        expectedOutput = "Woman{name='Alice', surname='Smith', year=1980, iq=0, schedule=[[WEDNESDAY, [Club]], [FRIDAY, [Restaurant]], [SATURDAY, [Cafe]]]}";
        assertEquals(expectedOutput, woman.toString());
    }
}
