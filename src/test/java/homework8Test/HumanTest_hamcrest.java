package homework8Test;

import org.homework8.*;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class HumanTest_hamcrest {

    Family family = TestUtilities.setUpFamily1();

    @Test
    public void testSetFamily() {
        Human human3 = new Human("Alex", "Doe", 2000);

        // Assert that setFamily returns true and the family is equal to the expected family
        assertThat(human3.setFamily(family), is(true));
        assertThat(human3.getFamily(), is(equalTo(family)));

        Family family2 = new Family(new Man("Bob", "Smith", 1990), new Woman("Alice", "Smith", 1995));

        // Assert that setFamily returns false and the family remains unchanged
        assertThat(human3.setFamily(family2), is(false));
        assertThat(human3.getFamily(), is(equalTo(family))); // Family should remain unchanged
    }

    @Test
    public void testFeedPetWhenTimeToFeed() {
        Dog pet = new Dog("Buddy", 4);
        pet.setTrickLevel(52);

        family.setPets(pet);

        // Test feeding when it's time to feed
        assertThat(family.getFather().feedPet(true), is(true));
    }

    @Test
    public void testFeedPet_shouldReturnTrueOrFalse() {
        DomesticCat cat = new DomesticCat("Whiskers", 3);
        // Assuming setTrickLevel generates a random value between 0 and 100
        cat.setTrickLevel(50);

        family.setPets(cat);

        // Now, instead of expecting a specific result, you can assert that it's either true or false
        assertThat(family.getFather().feedPet(false), anyOf(is(true), is(false)));
    }

    @Test
    public void testFeedPet() {
        // Arrange
        DomesticCat cat = new DomesticCat("Whiskers", 3);
        Family family = new Family(new Man("John", "Doe", 1970), new Woman("Jane", "Doe", 1975));
        family.setPets(cat);
        family.getFather().setFamily(family);

        // Retrieve the specific pet instance from the set
        Pet specificPet = family.getPets().iterator().next();

        // Test when it's not time to feed, and pet trick level is higher
        specificPet.setTrickLevel(100);
        assertThat(family.getFather().feedPet(false), is(true));

        // Test when it's not time to feed, and pet trick level is small
        specificPet.setTrickLevel(0);
        assertThat(family.getFather().feedPet(false), is(false));
    }

    @Test
    public void testGreetPet() {
        Human child = new Human("Alex", "Doe", 2000);
        family.addChild(child);

        DomesticCat cat = new DomesticCat("Whiskers", 3);
        family.setPets(cat);

        assertThat(family.getMother().greetPet(), is(equalTo("Hello, I greet you Whiskers!")));
        assertThat(family.getFather().greetPet(), is(equalTo("Hello, my favorite friend Whiskers!")));
        assertThat(child.greetPet(), is(equalTo("Hello, Whiskers!")));

        // Test greeting when there is no pet
        Family new_family = new Family(new Man("John", "Smith", 1970), new Woman("Jane", "Smith", 1975));
        Human child_new_family = new Human("Teresa", "Smith", 2000);
        new_family.addChild(child_new_family);
        new_family.setPets(null);

        assertThat(new_family.getMother().greetPet(), is(equalTo("I don't have a pet.")));
        assertThat(new_family.getFather().greetPet(), is(equalTo("I don't have a pet.")));
        assertThat(child_new_family.greetPet(), is(equalTo("I don't have a pet.")));
    }

    @Test
    public void testDescribePet() {
        Dog pet = new Dog("Buddy", 3); // Assuming Dog is a subclass of Pet
        pet.setTrickLevel(52);
        family.setPets(pet);

        // Test describing the pet
        String describePet = family.getMother().describePet();
        assertThat(describePet, is(equalTo("I have a DOG. It is 3 years old, and it is very cunning.")));

        // Test describing when there is no pet
        Family new_family = new Family(new Man("John", "Smith", 1970), new Woman("Jane", "Smith", 1975));
        Human child_new_family = new Human("Teresa", "Smith", 2000);
        new_family.addChild(child_new_family);
        new_family.setPets(null);

        assertThat(new_family.getMother().describePet(), is(equalTo("I don't have a pet.")));
        assertThat(new_family.getFather().describePet(), is(equalTo("I don't have a pet.")));
        assertThat(child_new_family.describePet(), is(equalTo("I don't have a pet.")));
    }

    @Test
    public void testRepairCar() {
        Man man = new Man("John", "Doe", 1980);

        // Redirect System.out to capture the printed output
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Call the repairCar() method
        man.repairCar();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the repairCar() method
        String expectedOutput = "I'm repairing the car.";
        String actualOutput = outputStreamCaptor.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput, is(equalTo(expectedOutput)));
    }

    @Test
    public void testMakeUp() {
        Woman woman = new Woman("Alice", "Smith", 1980);

        // Redirect System.out to capture the printed output
        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        // Call the makeup() method
        woman.makeup();

        // Reset the standard output stream
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        // Check only the output related to the makeup() method
        String expectedOutput = "I'm applying makeup.";
        String actualOutput = outputStreamCaptor.toString().trim();  // Remove leading/trailing whitespaces

        assertThat(actualOutput, is(equalTo(expectedOutput)));
    }

    @Test
    public void testEqualsAndHashCode() {
        Man human1 = new Man("John", "Doe", 1980);
        Man human2 = new Man("John", "Doe", 1980);
        Man human3 = new Man("Jane", "Doe", 1985);

        assertThat(human1, is(equalTo(human2)));
        assertThat(human1, is(not(equalTo(human3))));

        assertThat(human1.hashCode(), is(equalTo(human2.hashCode())));
        assertThat(human1.hashCode(), is(not(equalTo(human3.hashCode()))));
    }

    @Test
    public void testToString() {
        Family family = new Family(new Man("Charlie", "Brown", 1990),
                new Woman("Lucy", "Brown", 1992));

        Human human = new Human("Bryan", "Brown", 2010, 120, family,
                DayOfWeek.TUESDAY.name(), "Studying", DayOfWeek.SATURDAY.name(), "Gym");

        String expectedOutput = "Human{name='Bryan', surname='Brown', year=2010, iq=120, schedule=[[TUESDAY, [Studying]], [SATURDAY, [Gym]]]}";
        assertThat(human.toString(), is(equalTo(expectedOutput)));

        Man man = new Man("John", "Doe", 1980);
        man.setIQ(120);

        // Test the string representation of the man
        expectedOutput = "Man{name='John', surname='Doe', year=1980, iq=120, schedule=[]}";
        assertThat(man.toString(), is(equalTo(expectedOutput)));

        Woman woman = new Woman("Alice", "Smith", 1980);
        woman.setSchedule(new HashMap<>(
                Map.of(
                        DayOfWeek.WEDNESDAY, List.of("Club"),
                        DayOfWeek.SATURDAY, List.of("Cafe")
                )
        ));
        woman.addDayToSchedule(DayOfWeek.FRIDAY, "Restaurant");

        expectedOutput = "Woman{name='Alice', surname='Smith', year=1980, iq=0, schedule=[[WEDNESDAY, [Club]], [FRIDAY, [Restaurant]], [SATURDAY, [Cafe]]]}";
        assertThat(woman.toString(), is(equalTo(expectedOutput)));
    }
}
