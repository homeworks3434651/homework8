package homework8Test;

import org.assertj.core.api.Assertions;
import org.homework8.*;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

public class FamilyTest_assertj {

    Family family = TestUtilities.setUpFamily1();

    @Test
    public void testAddChildToFamily() {
        Human child = new Human("Bob", "Doe", 1995);

        assertThat(family.addChild(child))
                .as("Adding a child to the family should return true")
                .isTrue();

        assertThat(family.getChildren())
                .as("The family should contain the added child")
                .contains(child);

        assertThat(family.getChildren())
                .as("The family should have one child after adding")
                .hasSize(1);

        assertThat(family.getChildren().get(0))
                .as("The added child should be the specified child")
                .isSameAs(child);

        assertThat(child.getFamily())
                .as("The family reference of the child should be set correctly")
                .isSameAs(family);
    }

    @Test
    public void addChildShouldNotAddDuplicateChild() {
        Human child = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child))
                .as("Adding a unique child should return true")
                .isTrue();

        assertThat(family.addChild(child))
                .as("Adding a duplicate child should return false")
                .isFalse();
    }

    @Test
    public void deleteChildShouldRemoveChildFromFamily() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child1))
                .as("Adding the first child should return true")
                .isTrue();

        assertThat(family.addChild(child2))
                .as("Adding the second child should return true")
                .isTrue();

        // Check that the child is removed from the array
        family.deleteChild(child1);

        assertThat(family.getChildren())
                .as("The family should not contain the deleted child")
                .doesNotContain(child1);

        // Check that the array remains unchanged if a non-equivalent object is passed
        Human nonExistentChild = new Human("Non", "Existent", 1998);
        int originalChildrenCount = family.getChildren().size();

        family.deleteChild(nonExistentChild);

        assertThat(family.getChildren())
                .as("The family should remain unchanged if a non-existent child is passed")
                .hasSize(originalChildrenCount);
    }

    @Test
    public void deleteChildByIndexShouldRemoveChildFromFamily() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child1))
                .as("Adding the first child should return true")
                .isTrue();

        assertThat(family.addChild(child2))
                .as("Adding the second child should return true")
                .isTrue();

        // Check that the child is removed from the array
        assertThat(family.deleteChild(0))
                .as("Deleting the child at index 0 should return true")
                .isTrue();

        assertThat(family.getChildren())
                .as("The family should not contain the deleted child")
                .doesNotContain(child1);

        // Check that the array remains unchanged if an out-of-range index is passed
        assertThat(family.deleteChild(2))
                .as("Deleting a child with an out-of-range index should return false")
                .isFalse();

        // Check that the array remains unchanged and the method returns false
        assertThat(family.getChildren())
                .as("The family should remain unchanged if an out-of-range index is passed")
                .hasSize(1);
    }

    @Test
    public void testCountFamily() {
        assertThat(family.countFamily())
                .as("Initial family count should be 2")
                .isEqualTo(2);

        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child1))
                .as("Adding the first child should return true")
                .isTrue();

        assertThat(family.addChild(child2))
                .as("Adding the second child should return true")
                .isTrue();

        assertThat(family.countFamily())
                .as("Family count after adding two children should be 4")
                .isEqualTo(4);

        family.deleteChild(child1);

        assertThat(family.countFamily())
                .as("Family count after deleting one child should be 3")
                .isEqualTo(3);
    }

    @Test
    public void testEqualsAndHashCode() {
        Man father2 = new Man("John", "Doe", 1970);
        Woman mother2 = new Woman("Jane", "Doe", 1975);
        Family family2 = new Family(father2, mother2);
        Man father3 = new Man("Charlie", "Brown", 1990);
        Woman mother3 = new Woman("Lucy", "Brown", 1992);
        Family family3 = new Family(father3, mother3);

        // Testing reflexivity
        Assertions.assertThat(family).isEqualTo(family);

        // Testing consistency
        Assertions.assertThat(family).isEqualTo(family2);

        // Testing symmetry
        Assertions.assertThat(family2).isEqualTo(family);

        // Testing transitivity
        Assertions.assertThat(family).isEqualTo(family2).isEqualTo(family);

        // Testing equality with null
        Assertions.assertThat(family).isNotEqualTo(null);

        // Testing hash code consistency
        Assertions.assertThat(family.hashCode()).isEqualTo(family2.hashCode());

        // Testing hash code inequality with different objects
        Assertions.assertThat(family.hashCode()).isNotEqualTo(family3.hashCode());
        Assertions.assertThat(family).isEqualTo(family2);

        Man father4 = new Man("Charlie", "Brown", 1990);
        Woman mother4 = new Woman("Lucy", "Brown", 1992);
        Family family4 = new Family(father4, mother4);
        Human sonFamily3 = new Human("Bryan", "Brown", 2010);
        family3.addChild(sonFamily3);

        // Update: Using containsExactly for correct comparison
        Assertions.assertThat(family3.getChildren()).containsExactly(sonFamily3);

        Assertions.assertThat(family3).isNotEqualTo(family4);
    }

    @Test
    public void testSetChildren() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        // Act
        family.setChildren(List.of(child1, child2));

        // Assert
        assertThat(family.getChildren())
                .as("The family should contain the specified children")
                .contains(child1, child2);

        assertThat(child1.getFamily())
                .as("The family reference of child1 should be set correctly")
                .isEqualTo(family);

        assertThat(child2.getFamily())
                .as("The family reference of child2 should be set correctly")
                .isEqualTo(family);
    }

    @Test
    public void setChildrenShouldUpdateExistingChildrenList() {
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);
        family.addChild(child1);
        family.setChildren(List.of(child2));

        // Assert
        assertThat(family.getChildren())
                .as("The family's children list should contain both child1 and child2")
                .contains(child1, child2);

        assertThat(child2.getFamily())
                .as("The family reference of child2 should not be null")
                .isNotNull();

        assertThat(child1.getFamily())
                .as("The family reference of child1 should still be the family")
                .isEqualTo(family);
    }

    @Test
    public void setChildrenShouldHandleEmptyList() {
        // Act
        family.setChildren(Collections.emptyList());

        // Assert
        assertThat(family.getChildren())
                .as("The family should have an empty list of children")
                .isEmpty();
    }

    @Test
    public void testToString() {
        Human child1 = new Human("Bob", "Doe", 2005);
        Human child2 = new Human("Alice", "Doe", 2010);
        family.addChild(child1);
        family.addChild(child2);

        Pet myPet = new Dog("Buddy", 4);
        family.setPets(myPet);

        // Act
        String familyString = family.toString();

        // Assert
        String expectedOutput = "Family{\n" +
                "  Father: " + family.getFather() + "\n" +
                "  Mother: " + family.getMother() + "\n" +
                "  Children: [" + child1 + ", " + child2 + "]\n" +
                "  Pets: [" + myPet + "]\n" +  // Represent Pets as a list
                "  Total Persons in Family: 4\n" +
                "}";

        assertThat(familyString)
                .as("The string representation of the family should match the expected output")
                .isEqualTo(expectedOutput);
    }

    @Test
    public void testToStringWithNoChildrenAndNoPet() {
        String familyString = family.toString();

        // Assert
        String expectedOutput = "Family{\n" +
                "  Father: " + family.getFather() + "\n" +
                "  Mother: " + family.getMother() + "\n" +
                "  Children: []\n" +
                "  Pets: []\n" +
                "  Total Persons in Family: 2\n" +
                "}";

        assertThat(familyString)
                .as("The string representation of the family should match the expected output")
                .isEqualTo(expectedOutput);
    }
}
