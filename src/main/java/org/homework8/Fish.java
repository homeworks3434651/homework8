package org.homework8;

import java.util.TreeSet;

public class Fish extends Pet {

    public Fish(String nickname, int age) {
        super(nickname, age);
        super.setTrickLevel(0);
        super.setHabits(new TreeSet<>(super.getHabits()));
        setSpecies(Species.FISH);
    }

    @Override
    public void respond() {
        System.out.println("...");
    }

    @Override
    public void eat() {
        System.out.println("I eat fish food.");
    }
}
