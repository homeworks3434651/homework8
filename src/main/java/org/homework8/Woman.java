package org.homework8;

import java.util.*;

public final class Woman extends Human {

    public Woman() {
        super();
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, Family family) {
        super(name, surname, year, family);
    }

    public Woman(String name, String surname, Family family, int iq) {
        super(name, surname, family, iq);
    }

    public Woman(String name, String surname, int year, int iq, Family family, String... schedule) {
        super(name, surname, year, iq, family, schedule);

        // Set the family for the mother
        if (family != null) {
            family.setMother(this);
        }
    }

    public Woman(String name, String surname, int year, int iq, Family family, Map<DayOfWeek, List<String>> schedule) {
        super(name, surname, year, iq, family, schedule);

        // Set the family for the mother
        if (family != null) {
            family.setMother(this);
        }
    }
    @Override
    public String greetPet() {
        if (this.getFamily() != null && this.getFamily().getPets() != null && !this.getFamily().getPets().isEmpty()) {
            StringBuilder greetingBuilder = new StringBuilder("Hello, I greet you ");

            for (Pet petMember : this.getFamily().getPets()) {
                greetingBuilder.append(petMember.getNickname()).append("!");
            }

            return greetingBuilder.toString();
        } else {
            return "I don't have a pet.";
        }
    }

    public void makeup() {
        System.out.println("I'm applying makeup.");
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + getName() + '\'' +
                ", surname='" + getSurname() + '\'' +
                ", year=" + getYear() +
                ", iq=" + getIQ() +
                ", schedule=" + buildScheduleString(sortScheduleByDays(getSchedule())) +
                '}';
    }
}
