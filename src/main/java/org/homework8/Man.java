package org.homework8;

import java.util.List;
import java.util.Map;

public final class Man extends Human {

    public Man() {
        super();
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, Family family) {
        super(name, surname, year, family);
    }

    public Man(String name, String surname, Family family, int iq) {
        super(name, surname, family, iq);
    }

    public Man(String name, String surname, int year, int iq, Family family, String... schedule) {
        super(name, surname, year, iq, family, schedule);
    }

    public Man(String name, String surname, int year, int iq, Family family, Map<DayOfWeek, List<String>> schedule) {
        super(name, surname, year, iq, family, schedule);
    }

    @Override
    public String greetPet() {
        if (this.getFamily() != null && this.getFamily().getPets() != null && !this.getFamily().getPets().isEmpty()) {
            StringBuilder greetingBuilder = new StringBuilder("Hello, my favorite friend ");

            for (Pet petMember : this.getFamily().getPets()) {
                greetingBuilder.append(petMember.getNickname()).append("!");
            }

            return greetingBuilder.toString();
        } else {
            return "I don't have a pet.";
        }
    }

    public void repairCar() {
        System.out.println("I'm repairing the car.");
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + getName() + '\'' +
                ", surname='" + getSurname() + '\'' +
                ", year=" + getYear() +
                ", iq=" + getIQ() +
                ", schedule=" + buildScheduleString(sortScheduleByDays(getSchedule())) +
                '}';
    }
}
