package org.homework8;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FamilyService {

    private final CollectionFamilyDao familyDao;
    public static final int currentYear = Calendar.getInstance().get(Calendar.YEAR);

    public FamilyService(CollectionFamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        List<Family> allFamilies = this.familyDao.getAllFamilies();
        if (allFamilies.isEmpty()) {
            System.out.println("The list of families is empty.");
        }
        return allFamilies;
    }

    public List<Family> getAllFamilies(Predicate<Family> predicate) {
        return getAllFamilies().stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < getAllFamilies().size()) {
            return familyDao.getFamilyByIndex(index);
        }
        return null;
    }

    public boolean deleteFamily(int index) {
        return familyDao.deleteFamily(index);
    }

    public boolean deleteFamily(Family family) {
        if (getAllFamilies().isEmpty()) {
            System.out.println("The list of families is empty.");
            return false;
        }
        return familyDao.deleteFamily(family);
    }

    public void saveFamily(Family family) {
        if (family == null) {
            System.out.println("Cannot save null family.");
            return;
        }

        if (getAllFamilies().isEmpty()) {
            familyDao.saveFamily(family);
            System.out.println("Family added successfully.");
        } else if (getAllFamilies().contains(family)) {
            // Check if any child is already part of another family
            for (Human child : family.getChildren()) {
                if (child.getFamily() != null && !child.getFamily().equals(family)) {
                    System.out.println("Family didn't update.");
                    return;
                } else {
                    familyDao.saveFamily(family);
                    System.out.println("Family updated successfully.");
                }
            }
        } else {
            familyDao.saveFamily(family);
            System.out.println("Family added successfully.");
        }
    }

    public void displayAllFamilies() {
        System.out.println();
        System.out.println("Displaying all families:");
        getAllFamilies().forEach(System.out::println);
        System.out.println();
    }

    public List<Family> getFamiliesBiggerThan(int numberOfMembers) {
        List<Family> result = getAllFamilies(family -> family.countFamily() > numberOfMembers);
        printResult(result);
        return result;
    }

    public List<Family> getFamiliesLessThan(int numberOfMembers) {
        List<Family> result = getAllFamilies(family -> family.countFamily() < numberOfMembers);
        printResult(result);
        return result;
    }

    private void printResult(List<Family> result) {
        if (result.isEmpty()) {
            System.out.println("No families found satisfying the condition.");
        } else {
            System.out.println(result);
        }
    }

    public int countFamiliesWithMemberNumber(int numberOfMembers) {
        return getAllFamilies(family -> family.countFamily() == numberOfMembers).size();
    }

    public void createNewFamily(Man father, Woman mother) {
        Family newFamily = new Family(father, mother);
        getAllFamilies().add(newFamily);
    }

    public void deleteFamilyByIndex(int index) {
        if (index >= 0 && index < getAllFamilies().size()) {
            getAllFamilies().remove(index);
        } else {
            System.out.printf("There's no %d in the family list.%n", index);
        }
    }

    public static final String[] MALE_NAMES = {"John", "Bob", "Charlie", "Jack", "Landon"};
    public static final String[] FEMALE_NAMES = {"Alice", "Eva", "Olivia", "Deborah", "Beata"};

    public static int calculateInheritedIQ(Man father, Woman mother, Human child) {
        validateAndAdjustIQ(father, mother);

        int fatherIQ = father.getIQ();
        int motherIQ = mother.getIQ();

        // If the child's IQ is set, return that value
        if (child != null && child.getIQ() > 0 && child.getIQ() != 110) {
            return child.getIQ();
        }

        return (fatherIQ + motherIQ) / 2;
    }

    private static void validateAndAdjustIQ(Man father, Woman mother) {
        if (father.getIQ() == 0 && mother.getIQ() == 0) {
            System.out.println("Error: At least one parent's IQ must be specified.");
            father.setIQ(110);
            mother.setIQ(110);
            return;
        }

        if (father.getIQ() == 0) {
            System.out.println("Warning: Father's IQ is not specified. Using mother's IQ for the child.");
            father.setIQ(mother.getIQ());
        }

        if (mother.getIQ() == 0) {
            System.out.println("Warning: Mother's IQ is not specified. Using father's IQ for the child.");
            mother.setIQ(father.getIQ());
        }
    }

    public Family bornChild(Family family, String maleName, String femaleName) {
        if (family == null) {
            System.out.println("Warning: Family is not specified. Creating a new family for the child.");
        }

        Man father = family.getFather();
        Woman mother = family.getMother();
        Human child = new Human();

        if (father == null || mother == null) {
            System.out.println("Error: Father or mother is missing in the family.");
            return null;
        }

        if (child == null) {
            System.out.println("Error: Child can't be null.");
            return null;
        }

        String childName;

        // Determine the gender randomly (50% chance for Man or Woman)
        boolean isMale = new Random().nextBoolean();

        int inheritedIQ = calculateInheritedIQ(family.getFather(), family.getMother(), child);

        if (isMale) {
            childName = (maleName != null && !maleName.isEmpty()) ? maleName : getRandomMaleName();
            family.addChild(new Man(childName, father.getSurname(), family, inheritedIQ));
        } else {
            childName = (femaleName != null && !femaleName.isEmpty()) ? femaleName : getRandomFemaleName();
            family.addChild(new Woman(childName, father.getSurname(), family, inheritedIQ));
        }
        saveFamily(family);
        return family;
    }

    private static final Random random = new Random();

    public static String getRandomMaleName() {
        return MALE_NAMES[random.nextInt(MALE_NAMES.length)];
    }

    public static String getRandomFemaleName() {
        return FEMALE_NAMES[random.nextInt(FEMALE_NAMES.length)];
    }

    public Family adoptChild(Family family, Human child) {
        if (getAllFamilies().contains(family)) {
            // Check if the child is already part of another family
            if (child.getFamily() != null && !child.getFamily().equals(family)) {
                System.out.println("Error: This person is already part of another family.");
                System.out.println("Child adoption failed.");
                return null;
            }
            child.setSurname(family.getFather().getSurname());
            family.addChild(child);
            saveFamily(family);
            return family;
        } else {
            System.out.println("Error: Family not found.");
            System.out.println("Child adoption failed.");
            return null;
        }
    }

    public void deleteAllChildrenOlderThan(int age) {
        for (Family family : getAllFamilies()) {
            for (Human child : new ArrayList<>(family.getChildren())) {
                if (FamilyService.currentYear - child.getYear() > age) {
                    family.deleteChild(child);
                }
            }
            saveFamily(family);
        }
    }

    public int count() {
        return getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        if (index >= 0 && index < getAllFamilies().size()) {
            Family existingFamily = getAllFamilies().get(index);
            if (existingFamily != null) {
                return existingFamily;
            } else {
                return null;
            }
        } else {
            System.out.printf("Invalid index: %d. Family not found.%n", index);
            return null;
        }
    }

    public List<Pet> getPets(int index) {
        return index >= 0 && index < getAllFamilies().size()
                ? Optional.ofNullable(getAllFamilies().get(index))
                .map(Family::getPets)
                .orElseGet(Collections::emptySet)
                .stream()
                .collect(Collectors.toList()) // Collect to List instead of Set
                : Collections.emptyList();
    }

    public void addPet(int index, Pet pet) {
        if (index >= 0 && index < getAllFamilies().size()) {
            if (getFamilyById(index) != null) {
                Family family = getAllFamilies().get(index);
                family.setPets(pet);
                saveFamily(family);
            } else {
                System.out.println("Family at index " + index + " is null. Cannot add pet.");
            }
        } else {
            System.out.println("Invalid index: " + index + ". Cannot add pet.");
        }
    }
}
