package org.homework8;

import java.util.*;
import java.util.stream.Collectors;

public enum DayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    private static final Map<DayOfWeek, List<String>> schedule = new HashMap<>();

    public static void addActivities(DayOfWeek day, String activity) {
        schedule.computeIfAbsent(day, k -> new ArrayList<>()).add(activity);
    }

    public static void mergeActivities(DayOfWeek day, List<String> newActivities) {
        schedule.put(day, newActivities.stream().distinct().collect(Collectors.toList()));
    }

    public static List<String> getActivitiesForDay(DayOfWeek day, Human familyMember) {
        Map<DayOfWeek, List<String>> schedule = familyMember.getSchedule();
        return Optional.ofNullable(schedule.get(day)).orElse(List.of("No activity specified"));
    }

    public static String getActivityForDay(DayOfWeek dayOfWeek, Human familyMember) {
        Map<DayOfWeek, List<String>> schedule = familyMember.getSchedule();
        List<String> activities = schedule.getOrDefault(dayOfWeek, Collections.emptyList());

        return activities.isEmpty() ?
                "No activity specified" :
                activities.stream().collect(Collectors.joining(", ", "", ""));
    }
}
