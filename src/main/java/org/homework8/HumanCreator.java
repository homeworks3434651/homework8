package org.homework8;

public interface HumanCreator {

    Human bornChild(String childName, String fatherSurname, Family family, int averageIQ);
}
